import React from 'react';
import { act, fireEvent, render, screen } from '@testing-library/react-native';

import App from './App';

describe('App', () => {
  test('renders correctly', () => {
    render(<App />);
    const element = screen.getByTestId('container');
    expect(element).toBeDefined();
  });

  test('check box action', async () => {
    render(<App />);
    const element = screen.getByTestId('checkBox');
    await act(async () => {
      await fireEvent(element, 'onValueChange', true);
      console.log(element.props.accessibilityState.checked);
      expect(element.props.accessibilityState.checked).toBeTruthy();
    });
  });
});