import React, { useState } from 'react';
import { View } from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import {Card} from '@organization/libs/ui';

export const App = () => {
  const [toggleCheckBox, setToggleCheckBox] = useState(false);

  return (
    <View
      testID="container"
      style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
    >
      <CheckBox
        testID='checkBox'
        disabled={false}
        value={toggleCheckBox}
        onValueChange={(newValue) => setToggleCheckBox(newValue)}
      />
      <Card />
    </View>
  );
};

export default App;
